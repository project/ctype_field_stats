
-- SUMMARY --

This module provide report of content type's field.

-- USE CASE --
If administrator want to know which content type have how many fields and how 
much data associate with that field so this module provide report of content 
type's field stats.

-- REQUIREMENTS --
  No

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:


-- CUSTOMIZATION --

* Access menu from here : admin/config/search/ctype-field-stats

-- CONTACT --

Current maintainers:
* Ankush Gautam https://www.drupal.org/u/agautam
  email : ankush.gautam@kelltontech.com
