<?php

/**
 * @file
 * Enables functionality for Content type field stats.
 */

/**
 * Implements hook_help().
 */
function ctype_field_stats_help($path, $arg) {
  switch ($path) {
    // Main module help for the node alias module.
    case 'admin/help#ctype_field_stats':
      $output = t('This module keeps history of Content type field stats');
      $output .= t('Some time user want to know Content type field stats.');
      return $output;
  }
}

/**
 * Implements hook_menu().
 */
function ctype_field_stats_menu() {
  $items = array();

  $items['admin/config/search/ctype-field-stats'] = array(
    'title' => 'Content type field stats',
    'description' => 'Content type field stats',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ctype_field_stats_form'),
    'access arguments' => array('access content type field stats'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function ctype_field_stats_permission() {
  return array(
    'access content type field stats' => array(
      'title' => t('Access content type field stats'),
      'description' => t('Access content type field stats'),
    ),
  );
}

/**
 * Implements hook_form().
 */
function ctype_field_stats_form($form, &$form_state) {
  $form['fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search Content'),
  );
  $node_type = node_type_get_names();
  $nodetype = array("" => "Select Section");
  foreach ($node_type as $key => $value) {
    $nodetype[$key] = ucwords($value);
  }
  $form['fieldset']['type'] = array(
    '#title' => t('Content Type'),
    '#type' => 'select',
    '#required' => 'TRUE',
    '#description' => t('Please Select Content type name'),
    '#options' => $nodetype,
    '#default_value' => isset($_GET['type']) ? $_GET['type'] : '',
  );

  $form['fieldset']['display_button'] = array(
    '#prefix' => '<div class="submit">',
    '#suffix' => l(t('Reset'), 'admin/config/search/ctype-field-stats', array('attributes' => array('class' => array('reset')))) . '</div">',
    '#type' => 'submit',
    '#value' => t('Search'),
    '#submit' => array('ctype_field_stats_form_submit'),
  );
  $ctype_field_stats_rows = ctype_field_stats_rows();
  $count = count($ctype_field_stats_rows['ctype_field_stats'][0]['#rows']);
  if ($count > 1) {
    $count = $count - 1;
  }
  $row_info = 'Total Field Count : ' . $count;
  $form['markup'] = array(
    '#prefix' => $row_info,
    '#markup' => drupal_render($ctype_field_stats_rows),
  );

  return $form;
}

/**
 * Implements ctype_field_stats_form_submit().
 */
function ctype_field_stats_form_submit($form, &$form_state) {

  $ctype = $form_state['values']['type'];
  drupal_goto('admin/config/search/ctype-field-stats', array(
    'query' => array(
      'type' => $ctype,
    ),
  )
  );
}

/**
 * Function returns list of ctype field stats.
 */
function ctype_field_stats_rows() {

  $rows = array();
  $parameters = drupal_get_query_parameters();
  $content_type = isset($parameters['type']) ? $parameters['type'] : '';
  $revision_enabled = FALSE;
  if (!empty($content_type) && in_array('revision', variable_get('node_options_' . $content_type))) {
    $revision_enabled = TRUE;
  }
  if (!empty($content_type)) {
    $header = array(
      t('Sl no.'),
      t('Associated Field'),
      t('Entity Type'),
      t('Data Rows'),
    );
    if ($revision_enabled) {
      $header[] = t('Revision Rows');
    }
    $sub_query = db_select('field_config_instance', 'f2');
    $sub_query->addField('f2', 'field_name');
    $sub_query->join('field_config_instance', 'f3', 'f3.bundle = f2.field_name');
    if (isset($content_type) && !empty($content_type)) {
      $sub_query->condition('f2.bundle', $content_type);
    }
    $sub_query_result = array_values($sub_query->execute()->fetchAll(PDO::FETCH_ASSOC));
    $query = db_select('field_config_instance', 'f1');
    $query->fields('f1');
    $query->condition('f1.deleted', 0);
    if (isset($content_type) && !empty($content_type)) {
      if (count($sub_query_result)) {
        $db_or = db_or();
        $db_or->condition('f1.bundle', $content_type);
        $db_or->condition('f1.bundle', $sub_query_result, 'in');
        $query->condition($db_or);
      }
      else {
        $query->condition('f1.bundle', $content_type);
      }
    }
    $query = $query->extend('PagerDefault')->limit(100);
    $result = $query->execute();
    $sn = 0;
    $i = 0;
    $row_count = 0;
    $row_revision_count = 0;
    $title_count = ctype_field_stats_title_existence($content_type);
    $rows[$i] = array(
      ++$sn,
      check_plain(t('title')),
      check_plain(t('node')),
      $title_count,
    );
    $row_count += $title_count;
    if ($revision_enabled) {
      $title_revision_count = ctype_field_stats_title_existence($content_type, TRUE);
      $rows[$i][] = $title_revision_count;
      $row_revision_count += $title_revision_count;
    }
    $i++;
    while ($record = $result->fetchObject()) {
      $field_name = $record->field_name;
      $bundle_info = ctype_field_stats_get_config_info($record->field_name);
      if ($bundle_info['bundle'] == $content_type) {
        $content_type = $content_type;
      }
      if ($bundle_info['bundle'] != $content_type) {
        $content_type = $bundle_info['bundle'];
      }
      $rows_value = ctype_field_stats_existence($record->field_name, $content_type);
      $rows[$i] = array(
        ++$sn,
        check_plain($field_name),
        check_plain($record->entity_type),
        $rows_value,
      );
      $row_count += $rows_value;
      if ($revision_enabled) {
        $revision_rows_value = ctype_field_stats_existence($record->field_name, $content_type, TRUE);
        $rows[$i][] = $revision_rows_value;
        $row_revision_count += $revision_rows_value;
      }
      $i++;
    }
    $rows[$i] = array(
      '',
      '',
      '<strong>Sub total</strong>',
      '<strong>' . $row_count . '</strong>',
    );
    if ($revision_enabled) {
      $rows[$i][] = '<strong>' . $row_revision_count . '</strong>';
    }
    $render_array['ctype_field_stats'] = array(
      array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#empty' => t('No Record found!'),
      ),
      array(
        '#theme' => 'pager',
      ),
    );

    return $render_array;
  }
}

/**
 * Helper function to get field count.
 */
function ctype_field_stats_existence($table_key, $bundle = NULL, $revision = FALSE) {
  $table = 'field_data_' . $table_key;
  if ($revision) {
    $table = 'field_revision_' . $table_key;
  }
  $result = db_select($table, 't');
  $result->fields('t', array('entity_id'));
  if (!empty($bundle)) {
    $result->condition('bundle', $bundle);
  }
  return $result->execute()->rowCount();
}

/**
 * Helper function to get title count.
 */
function ctype_field_stats_title_existence($bundle = NULL, $revision = FALSE) {
  $table = 'node';
  if ($revision) {
    $table = 'node_revision';
    $result = db_select($table, 'n');
    $result->fields('n', array('nid'));
    $result->join('node', 'n1', 'n1.nid = n.nid');
    if (!empty($bundle)) {
      $result->condition('n1.type', $bundle);
    }
    return $result->execute()->rowCount();
  }
  $result = db_select($table, 'n');
  $result->fields('n', array('nid'));
  if (!empty($bundle)) {
    $result->condition('type', $bundle);
  }
  return $result->execute()->rowCount();
}

/**
 * Helper function to get config instance info.
 */
function ctype_field_stats_get_config_info($field_name) {
  $query = db_select('field_config_instance', 'f');
  $query->fields('f');
  $query->condition('field_name', $field_name);
  $result = $query->execute();
  while ($record = $result->fetchAssoc()) {
    $config_info['entity_type'] = $record['entity_type'];
    $config_info['bundle'] = $record['bundle'];
  }

  return $config_info;
}
